#ifndef SoundCard_h
#define SoundCard_h

#include <inttypes.h>
#include "Print.h"

class SoundCard {

public:
	SoundCard();
	void setVolume(int8_t vol);
	void playWithVolumeSoundFileNoFromTFcard(int16_t dat);
	void playSoundFileNoFromTFcard(int16_t dat);
	void cyclePlay(int16_t index);
	void setCyleMode(int8_t AllSingle);
	void playCombine(int8_t song[][2], int8_t number);
	void sendCommand(int8_t command, int16_t dat = 0);
	void mp3Basic(int8_t command);
	void mp3_5bytes(int8_t command, uint8_t dat);
	void mp3_6bytes(int8_t command, int16_t dat);
	void sendBytes(uint8_t nbytes);

};

#endif
